<?php
if($nome!=''){
	if($exec=='buscar'){ 
		include 'templates/busca.php'; 
	}
    else if ($exec=='listar'){
		include 'templates/form_select_titulo.php';
		BuscaMaterias($post_secao,'templates/listar.php');
	}
	else if ($exec=='editar') {
		include 'templates/update_materia.php';  
	}
	else if ($exec=='cad_materia'){
		include 'templates/insert_materia.php';  
	}
	else if ($exec=='insert_materia'){
		$data_hora=$ano.$mes.$dia.$hora.$min;
		cadastra_materia($post_secao,$titulo,$resumo,$comentario,$texto,$imagem1,$imagem2,$autor,$link,$dia,$mes,$ano,$hora,$min,$status,$data_hora);
		echo '<script> alert("Materia cadastrada com sucesso!"); </script>';
		echo '<script> window.location="sistema.php?exec=cad_materia";</script>';
	}
	else if ($exec=='update_materia'){
		$data_hora=$ano.$mes.$dia.$hora.$min;
		update_materia($post_secao,$titulo,$resumo,$comentario,$texto,$imagem1,$imagem2,$autor,$link,$dia,$mes,$ano,$hora,$min,$status,$data_hora,$post_id);
		echo '<script> alert("Materia atualizada com sucesso!"); </script>';
		echo '<script> window.location="sistema.php?exec=editar&p='.$post_secao.'&id='.$post_id.'";</script>';
	}
	else if ($exec=='cad_secao' && $priv=='admin'){
		include 'templates/cad_secao.php';
	}
	else if ($exec=='insert_secao' && $priv=='admin'){
		if (!cadastra_secao($post_secao))
			echo '<script> alert("Secao ja cadastrada, tente novamente"); </script>';
		else 
			echo '<script> alert("Cadastro efetuado com sucesso!"); </script>';
		echo '<script> window.location="sistema.php?exec=cad_secao";</script>';
	}
	else if($exec=='del_materia' && $priv=='admin'){
		include 'templates/del_materia.php';
	}
	else if($exec=='apagar_mat' && $priv=='admin'){
		if ($get_secao==''){
			include 'templates/form_select_del.php';
			BuscaMaterias($post_secao,'templates/apaga_materia.php');
		}
		else if ($get_secao!='' && $priv=='admin'){
			apagar_mat($get_secao,$get_id);
			echo "<script> ErrorMessage('3'); </script>";
		}
	}
	else if($exec=='del_secao' && $priv=='admin'){
			include 'templates/del_secao.php';
	}
	else if($exec=='apagar_secao' && $priv=='admin'){
		apagar_secao($post_secao);
		echo "<script> ErrorMessage('2') </script>";
	}
	else if($exec=='adm_usuarios' && $priv=='admin'){
		include 'templates/adm_users.php';
	}
	else if($exec=='flt_select_materia'){
		include 'templates/form_select_titulo.php';
		BuscaMateriasLimite($post_secao,$titulo,'templates/listar.php',$max);
	}
	else if($exec=='flt_delete_materia' && $priv=='admin'){
		include 'templates/form_select_del.php';
		BuscaMateriasLimite($post_secao,$titulo,'templates/apaga_materia.php',$max);
	}
	else if($exec=='cria_galeria'){
		CriaSecaoGaleria();
		include 'templates/cria_galeria.php';
		if($titulo!=''){
			$data_hora=$ano.$mes.$dia.$hora.$min;
			CadastraAlbum($titulo,$resumo,$autor,$imagem1,$imagem2,$link,$status,$dia,$mes,$ano,$hora,$min,$data_hora);
		}
	}
	else if($exec=='edita_galeria'){
		CriaSecaoGaleria();
		if($titulo=='')
			include 'templates/busca_galeria.php';
		else if($titulo!=''){
			include 'templates/edit_galeria.php';
		}
	}
	else if($exec=='up_galeria'){
		$data_hora=$ano.$mes.$dia.$hora.$min;
		UpdateAlbum($titulo,$resumo,$autor,$imagem1,$imagem2,$link,$status,$dia,$mes,$ano,$hora,$min,$data_hora);
		include 'templates/edit_galeria.php';
	}
	else if($exec=='exclui_galeria'){
		include 'templates/del_galeria.php';
		if($titulo!=''){
			ExcluiAlbum($titulo);
			$titulo='';
			echo '<script> document.location="sistema.php?exec=exclui_galeria"; </script>';
		}
	}
	
	//OP��ES DA ENQUETE
	else if($exec=='cria_enquete'){
		CriaSecaoEnquete();
		include 'templates/cria_enquete.php';
		if($titulo!=''){
			$data_hora=$ano.$mes.$dia.$hora.$min;
			CadastraEnquete($titulo,$autor,$opcao1,$opcao2,$opcao3,$opcao4,$opcao5,$opcao6,$status,$controle,$dia,$mes,$ano,$hora,$min,$data_hora);
		}
	}
	else if($exec=='edita_enquete'){
		CriaSecaoEnquete();
		if($titulo=='')
			include 'templates/busca_enquete.php';
		else if($titulo!=''){
			include 'templates/edit_enquete.php';
		}
	}
	else if($exec=='up_enquete'){
		$data_hora=$ano.$mes.$dia.$hora.$min;
		UpdateEnquete($titulo,$autor,$opcao1,$opcao2,$opcao3,$opcao4,$opcao5,$opcao6,$status,$controle,$dia,$mes,$ano,$hora,$min,$data_hora,$post_id);
		include 'templates/edit_enquete.php';
	}
	else if($exec=='exclui_enquete'){
		include 'templates/del_enquete.php';
		if($post_id!=''){
			ExcluiEnquete($post_id);
			$post_id='';
			echo '<script> document.location="sistema.php?exec=exclui_enquete"; </script>';
		}
	}
}
?>