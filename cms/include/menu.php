<script>
function MenuAcao(div,img){
	if(document.getElementById(div).style.display=='block'){
		document.getElementById(div).style.display='none';
		document.getElementById(img).src='img/ico_mais.jpg';
	}
	else{
		document.getElementById(div).style.display='block';
		document.getElementById(img).src='img/ico_menos.jpg';
	}
}
</script>
<style>
.dMenuOcult{border-left:1px dashed #fff; width:127px; padding:0 0 0 4px; margin:0 0 0 4px; display:none;}
.dMenuOcult ul{margin:0; padding:0;}
.dMenuOcult li{list-style:none; background:#3b3b2f; width:126px; height:21px; margin-bottom:2px; text-align:left; overflow:hidden; padding:0 0 0 5px;}
</style>
<?php
session_start();
$user=$_SESSION["sess_user"];
$senha=$_SESSION["sess_senha"];
$nome=$_SESSION["sess_nome"];
$priv=$_SESSION["sess_priv"];
		
	if ($priv=='admin'){?>
    	<div class="dBotao">
        	<a href="javascript:MenuAcao('dSecao','iSecao');" class="cArial_11_fff"> 
            <img src="img/ico_mais.jpg" border="0" id="iSecao"/> Se&ccedil;&otilde;es</a>
        </div>
		<div class="dMenuOcult" id="dSecao">
        	<ul>
            	<li><a href="sistema.php?exec=cad_secao" class="cArial_11_fff" target="corpo">Cadastrar Se&ccedil;&atilde;o</a></li>
                <li><a href="sistema.php?exec=del_secao" class="cArial_11_fff" target="corpo">Excluir Se&ccedil;&atilde;o</a></li>
            </ul>
        </div>	
        <div class="dBotao">
        	<a href="javascript:MenuAcao('dMateria','iMateria');" class="cArial_11_fff"> 
            <img src="img/ico_mais.jpg" border="0" id="iMateria"/> Mat&eacute;rias</a>
        </div>
		<div class="dMenuOcult" id="dMateria">
        	<ul>
            	<li><a href="sistema.php?exec=cad_materia" class="cArial_11_fff" target="corpo">Cadastrar Mat&eacute;ria</a></li>
                <li><a href="sistema.php?exec=buscar" class="cArial_11_fff" target="corpo">Editar Mat&eacute;rias</a></li>
                <li><a href="sistema.php?exec=del_materia" class="cArial_11_fff" target="corpo">Excluir Mat&eacute;rias</a></li>
            </ul>
        </div>
        <div class="dBotao">
        	<a href="javascript:MenuAcao('dGaleria','iGaleria');" class="cArial_11_fff"> 
            <img src="img/ico_mais.jpg" border="0" id="iGaleria"/> Galeria de Fotos</a>
        </div>
		<div class="dMenuOcult" id="dGaleria">
        	<ul>
            	<li><a href="sistema.php?exec=cria_galeria" class="cArial_11_fff" target="corpo">Criar galeria</a></li>
                <li><a href="sistema.php?exec=edita_galeria" class="cArial_11_fff" target="corpo">Editar galeria</a></li>
                <li><a href="sistema.php?exec=exclui_galeria" class="cArial_11_fff" target="corpo">Excluir galeria</a></li>
            </ul>
        </div>
        <div class="dBotao">
        	<a href="javascript:MenuAcao('dEnquete','iEnquete');" class="cArial_11_fff"> 
            <img src="img/ico_mais.jpg" border="0" id="iEnquete"/> Enquete</a>
        </div>
		<div class="dMenuOcult" id="dEnquete">
        	<ul>
            	<li><a href="sistema.php?exec=cria_enquete" class="cArial_11_fff" target="corpo">Criar enquete</a></li>
                <li><a href="sistema.php?exec=edita_enquete" class="cArial_11_fff" target="corpo">Editar enquete</a></li>
                <li><a href="sistema.php?exec=exclui_enquete" class="cArial_11_fff" target="corpo">Excluir enquete</a></li>
            </ul>
        </div>		
		<div class="dBotao"><a href="sistema.php?exec=adm_usuarios" class="cArial_11_fff" target="corpo">Administrar usu&aacute;rios</a></div>
       	<div class="dBotao">
        	<a href="javascript:void(0);" onclick="window.open('upload.php','FTP','width=758,height=605');" class="cArial_11_fff">
            	Enviar Arquivos
            </a>
        </div>
		<div class="dBotao"><a href="sair.php" class="cArial_11_fff" target="corpo">Sair</a></div>
	<?php } 
	else if ($priv=='user'){?>
    	<div class="dBotao">
        	<a href="javascript:MenuAcao('dMateria','iMateria');" class="cArial_11_fff"> 
            <img src="img/ico_mais.jpg" border="0" id="iMateria"/> Mat&eacute;rias</a>
        </div>
		<div class="dMenuOcult" id="dMateria">
        	<ul>
            	<li><a href="sistema.php?exec=cad_materia" class="cArial_11_fff" target="corpo">Cadastrar Mat&eacute;ria</a></li>
                <li><a href="sistema.php?exec=buscar" class="cArial_11_fff" target="corpo">Editar Mat&eacute;rias</a></li>
            </ul>
        </div>
        <div class="dBotao">
        	<a href="javascript:MenuAcao('dGaleria','iGaleria');" class="cArial_11_fff"> 
            <img src="img/ico_mais.jpg" border="0" id="iGaleria"/> Galeria de Fotos</a>
        </div>
		<div class="dMenuOcult" id="dGaleria">
        	<ul>
            	<li><a href="sistema.php?exec=cria_galeria" class="cArial_11_fff" target="corpo">Criar galeria</a></li>
                <li><a href="sistema.php?exec=edita_galeria" class="cArial_11_fff" target="corpo">Editar galeria</a></li>
            </ul>
        </div>
        <div class="dBotao">
        	<a href="javascript:MenuAcao('dEnquete','iEnquete');" class="cArial_11_fff"> 
            <img src="img/ico_mais.jpg" border="0" id="iEnquete"/> Enquete</a>
        </div>
		<div class="dMenuOcult" id="dEnquete">
        	<ul>
            	<li><a href="sistema.php?exec=cria_enquete" class="cArial_11_fff" target="corpo">Criar enquete</a></li>
                <li><a href="sistema.php?exec=edita_enquete" class="cArial_11_fff" target="corpo">Editar enquete</a></li>
            </ul>
        </div>
       	<div class="dBotao">
        	<a href="javascript:void(0);" onclick="window.open('upload.php','FTP','width=758,height=605');" class="cArial_11_fff">
            	Enviar Arquivos
            </a>
        </div>
		<div class="dBotao"><a href="sair.php" class="cArial_11_fff" target="corpo">Sair</a></div>
<?php }?>