<?php
session_start();
if($user=$_SESSION["sess_user"]=='') header ("location:index.html");

$diretorio="../imagens/";//getcwd();
$ponteiro=opendir($diretorio);
while ($nome_itens=readdir($ponteiro)){
	$itens[]=$nome_itens;
}
sort($itens);
foreach($itens as $listar) {
	if ($listar!="." && $listar!=".."){
		if (is_dir($listar)) {
			$pastas[]=$listar; 		
		} 
		else{
			$arquivos[]=$listar;		
		}
	}
}
$subdir=$_POST['subdir'];
$w2box_name = "Postar - Upload";
$storage_dir = "../imagens/"; // storage directory (chmod 777)
if($subdir!='1' && $subdir!='')$storage_dir.=$subdir;
$max_filesize=1000 * pow(1024,2); // maximum filesize for this script (x MiB), update post_max_size & upload_max_filesize in php.ini for big size
$allowed_fileext = array("gif","jpg","jpeg","png","pdf","txt","rar","zip");// allowed extensions


$auth = true; //if true no authentication, everyone allowed to do everything. 

//to login as admin when everything is hidden, click on "Powered" (hidden link) in the footer!!! 
$admin_user = "admin";
$admin_pass = "admin";
$protect_upload = true; //allow only admin to upload
$hide_upload = $protect_upload; //hide upload form if not admin
$protect_delete = true; //allow only admin to delete
$hide_delete = $protect_delete; //hide delete column if not admin

//login


//find real max_filesize
$max_filesize = min(return_bytes(ini_get('post_max_size')),return_bytes(ini_get('upload_max_filesize')),$max_filesize);

// deleting
if (isset($_POST["delete"])) {
	if ($protect_delete) authorize();
	deletefile($_POST["delete"]);
}
function deletefile($cell){
	global $storage_dir;
	//$storage_dir.="fotos";
	$cell=strip_tags($cell);
	$file=substr($cell,0,strlen($cell)-1);
	$file = "$storage_dir/".basename($file);

	if (!file_exists(utf8_decode($file)))
	echo "Error: Aruivo n�o encontrado. ($file)";
	else {
		$return = @unlink(utf8_decode($file));
		if ($return) echo "successful"; else echo "Error: permiss�o negada.";
	}
	exit;
}

//uploading
if (isset($_FILES['file'])) {
	if ($protect_upload) authorize();
	uploadfile($_FILES['file']);
}

function uploadfile($file) {
	global $storage_dir, $max_filesize, $allowed_fileext, $errormsg;

	if ($file['error']!=0) {
		switch ($file['error']) {
			case 1: $errormsg = 'O upload deste arquivo excede a diretiva "upload_max_filesize" em php.ini'; break;
			case 2: $errormsg = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form."; break;
			case 3: $errormsg = "The uploaded file was only partially uploaded."; break;
			//case 4: $errormsg = "No file was uploaded."; break;
			case 6: $errormsg = "Missing a temporary folder."; break;
		}
		return;
	}

	$filesource=$file['tmp_name'];

	$filename=$file['name'];
	if (isset($_POST['filename']) && $_POST['filename']!="") $filename=$_POST['filename'];
	$filename=str_replace(" ","_",$filename);
	if (!in_array(strtolower(extname($filename)), $allowed_fileext)) $filename .= ".badext";


	$filesize=$file['size'];
	if ($filesize > $max_filesize) {
		$errormsg = "Arquivo maior que o permitido (".getfilesize($max_filesize).").";
		return;
	}

	$filedest="$storage_dir/$filename";
	if (file_exists($filedest)) {
		$errormsg = 'Existe um arquivo "'.$filename.'" neste diret�rio.';
		return;
	}

	if (!copy($filesource,$filedest)) {
		$errormsg = "Permiss�o negada para gravar arquivos.";
	}

	if  ($errormsg=="") {
		Header("Location:".rooturl());
		exit;
	}
}

//downloading
if (isset($_GET['download']))
downloadfile($_GET['download']);

function downloadfile($file){
	global $storage_dir;
	$file = "$storage_dir/".basename($file);
	if (!is_file($file)) { return; }
	header("Content-Type: application/octet-stream");
	header("Content-Size: ".filesize($file));
	header("Content-Disposition: attachment; filename=\"".basename($file)."\"");
	header("Content-Length: ".filesize($file));
	header("Content-transfer-encoding: binary");
	@readfile($file);
	exit(0);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1" />
  <title><?php print $w2box_name; ?></title>
  <meta name="author" content="cb" />
  <meta name="description" content="w2box, web2.0 File Repository" />
  <meta name="keywords" content="upload, download, box, web2.0, ajax" />
  <meta name="content-language" content="en" />
  <link rel="stylesheet" type="text/css" href="css/ftp_file.css" />
  <script type="text/javascript" src="js/sorttable.js"></script>  
  <script type="text/javascript" src="js/pt.ajax.js"></script>
  <script type="text/javascript">
  <!--//<![CDATA[

  function deletefile(row) {
  	row.className='delete';
  	new Ajax.Request("upload.php", {
  		parameters: '&delete=' + encodeURIComponent(row.cells[0].innerHTML),
  		onComplete: function (req) {
  			if (req.responseText == "successful") {
  				row.parentNode.removeChild(row);
  			} else {
  				alert(req.responseText);
  				row.className='off';
  			}
  		}
  	});
  }

  function renameSync() {
  	var fn = document.getElementById("file").value;
  	if (fn == ""){
  		document.getElementById("filename").value = '';
  	} else {
  		var filename = fn.match(/[\/|\\]([^\\\/]+)$/);
  		if (filename==null) 
  		  filename = fn; //opera...
  		else
  		  filename = filename[1];
  		
  		document.getElementById("filename").value = filename;
  	}

  	filetypeCheck();
  }

  function filetypeCheck() {
  	var allowedtypes = '.<?php echo join(".",$allowed_fileext); ?>.';

  	var fn = document.getElementById("filename").value;
  	if (fn == ""){
  		document.getElementById("allowed").className ='';
  		document.getElementById("upload").disabled = true;
  	} else {
  		var ext = fn.split(".");
  		if (ext.length==1)
  		ext = '.noext.';
  		else
  		ext = '.' + ext[ext.length-1].toLowerCase() + '.';

  		if (allowedtypes.indexOf(ext) == -1) {
  			document.getElementById("allowed").className ='red';
  			document.getElementById("upload").disabled = true;
  		} else {
  			document.getElementById("allowed").className ='';
  			document.getElementById("upload").disabled = false;
  		}
  	}

  }
  
function CriarDiretorio(){
	if(document.getElementById('dir').value!=''){
		document.formdir.submit();
	}
	else alert("Informe o nome do diret�tio");
}

function MudarDiretorio(){
	document.formFile.submit();
}

  //]]>-->
</script>
</head>

<body onload="document.getElementById('upload').disabled = true;">
<div id="page">
<div id="header"><a href="upload.php" style="height: 100%;display: block;"></a></div>

<div id="content">
 	<div id="errormsg">
	<?php $pasta=$_POST["pasta"]; 
			if($pasta!=""){
				if(mkdir("$storage_dir$pasta", 0700 ))
					echo "Pasta ".$pasta." criada com sucesso!";
				else echo 'J� existe uma pasta chamada "'.$pasta.'" !';
			}
	?>
 	 <p class="red"><?php if (isset($errormsg)) {echo $errormsg;} ?></p>
 	</div>
<?php if ($demo) { ?>
<?php } ?>
<?php if (!$hide_upload || $auth) { ?>
 	<div id="uploadform">
		<form method="post" enctype="multipart/form-data" action="" name="formFile">
		<p><label for="file">Diret�rio</label>
			<select name="subdir" onchange="MudarDiretorio();">
				<?php if($_POST['subdir']=='' || $_POST['subdir']=='1'){?>
				<option value="1">Selecione uma pasta</option>
				<?php } else {?>
				<option value="<?php echo $_POST['subdir']; ?>"><?php echo $_POST['subdir']; ?></option>
				<option value="1">Voltar a pasta raiz</option>
				<?php }?>
	   			<?php listpastas($storage_dir);?>
			</select> *Para salvar no diret�rio raiz n�o altere essa op��o</p>
		<p><label for="file">Arquivo :</label>
			<input type="file" id="file" name="file" size="50" onchange="renameSync();" />
			<input id="upload" type="submit" value="Upload" class="button" /></p>
		<p><label for="filename">Renomear:</label>
			<input type="text" id="filename" name="filename" onkeyup="filetypeCheck();" size="50" /></p>
		<p class="small">
			<span id="allowed">Arquivos permitidos: <?php echo join(",",$allowed_fileext); ?></span><br />
			Tamanho m�ximo permitido: <?php echo getfilesize($max_filesize); ?></p>
		</form>
		<form name="formdir" action="" method="post" enctype="multipart/form-data" class="form">
			<label for="file">Criar Pasta:</label>
			<input type="text" id="dir" name="pasta" size="50"/>
			<input type="button" value="Criar pasta" class="button" onclick="CriarDiretorio();"/>
		</form>
 	</div>
<?php } ?>
	<div id="filelisting" class="dFiles">
	  <img src="img/arrow-up.gif" alt="" style="display:none;" /><img src="img/arrow-down.gif" alt="" style="display:none;" />		
		<?php listfiles($storage_dir); ?>
	</div>
</div>
<div id="footer"><p></div>

</div>
</body>
</html>
<?php

function listfiles($dir) {
	
?>
<table id="t1" class="sortable">
  <tr>
    <th id="th1" class="lefted">File Name</th>
    <th id="th2">Type</th>
    <th id="th3">Size</th>
<?php if (!$hide_delete || $auth) { ?>
    <th id="th4" class="unsortable">Delete</th>
<?php } ?>
  </tr>
<?php
if ($handle = opendir($dir)) {
	while (false !== ($file = readdir($handle))) {
		if ($file != "." && $file != ".."  && $file != "index.html") {
			$size=filesize($dir."/".$file);
			$ext=strtolower(extname($file));
			$file_ue=urlencode($file);
			$file_=substr($file_ue,-4,4);
			$file_=strtolower($file_);
			if($file_=='.jpg' || $file_=='.gif' || $file_=='.png' || $file_=='jpeg' || $file_=='.txt' || 
			$file_=='.rar' || $file_=='.pdf'){
				print("<tr class=\"off\" onmouseover=\"if (this.className!='delete') {this.className='on'};\" onmouseout=\"if (this.className!='delete') {this.className='off'};\">");
				print("<td class=\"lefted\"><a href=\"$dir/$file_ue\" target=\"_blank\">$file</a>");
				print(" <a href=\"?download=$file_ue\"><img src=\"img/download_arrow.gif\" alt=\"(download)\" title=\"Download Now!\" /></a></td>");
				print("<td>$ext</td>");
				//print("<td><a href=\"http://filext.com/detaillist.php?extdetail=$ext\">$ext</a></td>");
				print("<td>".getfilesize($size)."</td>");
				if (!$hide_delete || $auth) { 
			  		print("<td><a title=\"delete\" onclick=\"deletefile(this.parentNode.parentNode); return false;\" href=\"\"><img src=\"img/delete.gif\" alt=\"delete\" title=\"Delete\" /></a></td>");
				}
				print("</tr>\n");
			}
		}
	}
	closedir($handle);
}
?>
</table>
<?php
}

function listpastas($dir){
	if ($handle = opendir($dir)){
		while ($file=readdir($handle)){
			if ($file!= "." && $file != ".."  && $file != "index.html"){
				$file_ue=urlencode($file);
				$file_=substr($file_ue,-4,4);
				$file_=strtolower($file_);
				if($file_!='.jpg' && $file_!='.gif' && $file_!='.png' && $file_!='jpeg' && 
				$file_!='s.db' && $file_!='.txt' && $file_!='.rar' && $file_!='.pdf'){
					$file_ue=str_replace("+"," ",$file_ue);
					print '<option value="'.$file_ue.'">'.$file_ue.'</option>';
				}
			}
		}
		closedir($handle);
	}
}
function authorize($silent=false){
	global $auth,$admin_user,$admin_pass;
	//authentication
	if (!$auth){
		if ((isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) &&
		($_SERVER['PHP_AUTH_USER'] == $admin_user && $_SERVER['PHP_AUTH_PW']==$admin_pass)) {
			$auth = true; // user is authenticated
		} else {
			if (!$silent) {
				header( 'WWW-Authenticate: Basic realm="w2box admin"' );
				header( 'HTTP/1.0 401 Unauthorized' );
				echo 'Your are not allowed to access this function!';
				exit;
			}
		}
	}

}

function extname($file) {
	$file = explode(".",basename($file));
	return $file[count($file)-1];
}

function getfilesize($size) {
	if ($size < 2) return "$size byte";
	$units = array(' bytes', ' KB', ' MB', ' GB', ' TB');
	for ($i = 0; $size > 1024; $i++) { $size /= 1024; }
	return round($size, 2).$units[$i];
}

function return_bytes($val) {
	$val = trim($val);
	$last = strtolower($val{strlen($val)-1});
	switch($last) {
		// The 'G' modifier is available since PHP 5.1.0
		case 'g':
		$val *= 1024;
		case 'm':
		$val *= 1024;
		case 'k':
		$val *= 1024;
	}
	return $val;
}

function rooturl(){
	$dir = dirname($_SERVER['PHP_SELF']);
	if (strlen($dir) > 1) $dir.="/";
	return "upload.php";
	//return "http://".$_SERVER['HTTP_HOST'].$dir;
}
?>
