<?php
session_start();
$user=$_SESSION["sess_user"];
$senha=$_SESSION["sess_senha"];
$nome=$_SESSION["sess_nome"];
$priv=$_SESSION["sess_priv"];
include 'include/get_sistema.php';
include 'include/funcoes.php';
if(!validar($user,$senha)) echo '<script> window.location="login.php"; </script>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="css/estrutura.css" rel="stylesheet" type="text/css" />
<title>Untitled Document</title>

<script language="JavaScript" type="text/javascript" src="js/richtext.js"></script>
<script language="JavaScript" type="text/javascript" src="js/lightbox.js"></script>
<script language="JavaScript" type="text/javascript" >

function updateForm() {
		updateRTE('rte1');
		var rte1= document.form.rte1.value;
		document.getElementById('texto').value=rte1;
		document.form.submit();
		return false;
}

function submitForm() {
		updateRTE('rte1');
		var rte1= document.form.rte1.value;
		document.getElementById('texto').value=rte1;
		if (document.getElementById('cad_secao').value==0)
			alert("Selecione uma se��o");
		else{	
		document.form.submit();
		return false;}
}

initRTE("images/", "", "");
	
function mostrar(){
	var texto=document.getElementById('texto').value;
	writeRichText('rte1', texto, 757, 180, true, false);
}

function delMateria(secao,key){
		var llink="sistema.php?exec=apagar_mat&p="+secao+"&id="+key;
		if (confirm("Deseja realmente apagar esta mat�ria"))
		document.location=llink;
}

var div=new Array(255);
for(i=1;i<=255;i++){
	nome='menu'+i;
	div[nome]=1;
}

function menu(x){
	if (div[x]==0){
		document.getElementById(x).style.display='none'; 
		document.getElementById(x+'_link2').style.display='block';
		document.getElementById(x+'_link1').style.display='none';
		div[x]=1;
		}
	else if (div[x]==1){
		document.getElementById(x).style.display='block'; 
		document.getElementById(x+'_link2').style.display='none';
		document.getElementById(x+'_link1').style.display='block';
		div[x]=0;
		}
}

function setForm(x){
	if (x=='form1'){
		if(document.getElementById('form1_user').value!=0){
			var senha=document.getElementById('form1_senha').value;
			var csenha=document.getElementById('form1_csenha').value;
			if (senha!=csenha)
				alert ("A campo CONFIMA��O DE SENHA deve ser igual ao campo SENHA");
			else if(document.getElementById('form1_user').value=='admin' && document.getElementById('form1_priv').value=='user')
				alert("O Administrador n�o pode ter seu privil�gio alterado");
			else document.form1.submit();
		}else alert ("Voc� deve escolher um usu�rio");
	}
	if (x=='form2'){
		var senha=document.getElementById('form2_senha').value;
		var csenha=document.getElementById('form2_csenha').value;
		if(document.getElementById('form2_priv').value!=0){
			if (document.getElementById('form2_nome').value=='')
				alert("O Campo NOME deve ser preenchido");
			else if (document.getElementById('form2_user').value=='')
				alert("O Campo USER deve ser preenchido");				
			else if (senha!=csenha)
				alert ("A campo CONFIMA��O DE SENHA deve ser igual ao campo SENHA");
			else document.form2.submit();
		}else alert ("Voc� deve escolher o privil�gio");
	}
	if (x=='form3'){
		if(document.getElementById('form3_user').value==0)
			alert ("Escolha o usu�rio a ser excluido!");
		else if (confirm("Deseja realmente excluir este usu�rio?"))
		document.form3.submit();
	}
}
function setFormGaleria(){
	if(document.getElementById('iTit').value!=""){
		document.formGaleria.submit();
	}
	else alert("O campo TITULO deve ser preenchido");
}

function ErrorMessage(x){
	if(x==1){
		if(document.getElementById('sTitulo').value!=0){
			if(confirm("Deseja realmente apagar esta galeria?")){
				alert("Galeria Apagada com sucesso");
				document.form.submit();
			}
		}
		else alert("Selecione uma galeria");
	}
	if(x==2){
		alert("Se��o apagada com sucesso!");
		window.location="sistema.php?exec=del_secao";
	}
	if(x==3){
		alert("Mat�ria apagada com sucesso!");
		window.location="sistema.php?exec=del_materia";
	}
	if(x==4){
		if(document.getElementById('sTitulo').value!=0){
			if(confirm("Deseja realmente apagar esta enquete?")){
				alert("Enquete Apagada com sucesso");
				document.form.submit();
			}
		}
		else alert("Selecione uma Enquete");
	}
}
</script>
</head>

<body>
<div class="dPrincipal">
	<div class="dCorpoMenu">
		<span class="cArial_18_fff">MENU</span>
		<?php include 'include/menu.php'; ?>
  </div>
	<div class="dConteudo">
    	<div class="dData">
        	<span class="cArial_12_000">Brasil, <?php data(1); ?></span>
        </div>
        <?php 
			include 'include/if_sistema.php';
		?>
  </div>
</div>
</body>
</html>
