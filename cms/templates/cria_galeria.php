<style>
.dFormPrincipal{width:750px; height:383px; padding:24px; border:1px solid #000; margin:5px;}
.dFormPrimeiro, .dFormSegundo{width:755px; height:46px;}
.dFormTerceiro{width:755px; height:83px;}
.dFormQuarto{width:755px; height:260px;}
.dFormQuinto{width:755px; height:123px;}
.dFormSexto{width:755px; height:96px;}
.dFormEnviar{width:755px; height:48px; text-align:center; padding:35px 0 0 0;} 
.dFormInputTxt{width:377px; float:left; height:40px;}
.iText{width:342px; border:1px solid #000; color:#000; font-family:Arial; font-size:11px; font-weight:normal;}
.iDate{width:30px; border:1px solid #000; color:#000; font-family:Arial; font-size:11px; font-weight:normal; text-align:center;}
.iTit{width:755px; border:1px solid #000;}
.sSelect{width:342px; border:1px solid #000; color:#000; font-family:Arial; font-size:11px; font-weight:normal; margin:5px 0;}
.dResumo{width:755px; height:58px; border:1px solid #000;}
.dTexto{width:755px; height:238px; border:1px solid #000;}
.dComentario{width:755px; height:99px; border:1px solid #000;}
.cArialBold_11_000{font-family:Arial; font-size:11px; font-weight:bold; color:#000;}
</style>
<form action="sistema.php?exec=cria_galeria" method="post" name="formGaleria">
<div class="dFormPrincipal">
  <div class="dFormPrimeiro">
    	<div class="dFormInputTxt">
        	<span class="cArialBold_11_000">Data e hora da galeria <br />
       	  Data:</span>
            <input type="text" name="dia" class="iDate" value="<?php print date("d"); ?>"/> / 
			<input type="text" name="mes" class="iDate" value="<?php print date("m"); ?>"/> / 
			<input type="text" name="ano" class="iDate" value="<?php print date("Y"); ?>"/>
			<span class="cArialBold_11_000">Hora:</span> 
			<input type="text" name="hora" class="iDate" value="<?php print date("H"); ?>"/> : 
			<input type="text" name="min" class="iDate" value="<?php print date('i'); ?>"/>
      </div>
        <div class="dFormInputTxt">
        	<span style="padding-left:35px" class="cArialBold_11_000">Autor</span><br />
        	<input type="text" name="autor" class="iText" style="float:right"  value="<?php echo $nome; ?>"/>
        </div>
    </div>
    <div class="dFormSegundo">
    	<span class="cArialBold_11_000">Titulo da galeria </span><br />
        <input type="text" name="titulo" class="iTit" id="iTit"/>
    </div>
    <div class="dFormTerceiro">
    	<span class="cArialBold_11_000">Resumo da galeria </span><br />    
    	<textarea name="resumo" class="dResumo" ></textarea>
    </div>
    <div class="dFormSexto">
    	<div class="dFormInputTxt">
        	<span class="cArialBold_11_000">Foto Principal</span><br />
            <input type="text" name="imagem1" class="iText" />
        </div>
        <div class="dFormInputTxt">
        	<span style="padding-left:35px" class="cArialBold_11_000">Diretorio das fotos </span><br />
            <input type="text" name="imagem2" class="iText" style="float:right" /></div>
        <div class="dFormInputTxt">
        	<span class="cArialBold_11_000">Status da galeria </span><br />
            <select name="status" class="iText" >
				<option value="0">Aguardando aprova&ccedil;&atilde;o</option>
				<option value="1">Aprovada</option>
			</select>
        </div>
        <div class="dFormInputTxt">
        	<span style="padding-left:35px" class="cArialBold_11_000">Link da galeria</span><br />
            <input type="text" name="link" class="iText" style="float:right" />
        </div>
    </div>
    <div class="dFormEnviar">
    	<a href="javascript:setFormGaleria();"><img src="img/btn_enviar.gif" border="0" /></a>
    </div>
</div>
</form>