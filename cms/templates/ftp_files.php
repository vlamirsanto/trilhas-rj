<?php 
include '../include/config.php';
include '../include/funcoes.php';
$file=$_POST['file'];
$destino=$_POST['destino'];
if ($file!=''){
	$ftp=DIR.$destino;
	if (upload($file,$ftp)){
	$print=str_replace("/public_html/","",$ftp);
	$file="";
	}else echo '<script> alert("Erro durante transferência!"); </script>';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Postar - FTP</title>
<style>
body{margin:0px; padding:0px; background:#ece9d8;}
.dFtpPrincipal{width:350px; height:163px;}
.dFtpConteudo{height:129px; padding:15px;}
.cArialBold_14_000{font-family:Arial; font-size:14px; font-weight:bold; color:#000;}
.cArialBold_11_000{font-family:Arial; font-size:11px; font-weight:bold; color:#000;}
.iFile{width:300px;}
.dFtpStatus{border:1px solid #ece9d8; background:#ece9d8;}
</style>
<script>
function submitForm(){
	var file_true=document.getElementById('file_true');
	var file_false=document.getElementById('file_false');
	file_true.value=file_false.value.replace(/\\/g,"/");
	var file=file_true.value.replace( /.*\//, "" );
	document.getElementById("destino").value=file;
	document.getElementById('status').value="Enviando...";
	document.form.submit();
}
</script>
</head>
<body>
<div class="dFtpPrincipal">
	<div class="dFtpConteudo">
	<center><span class="cArialBold_14_000">Envio de Arquivos para o servidor</span></center>
	<form action="ftp_files.php" method="post" enctype="multipart/form-data" name="form">
		<span class="cArialBold_11_000">Selecione a imagem para upload:</span><br />
		<input type="file" id="file_false" value="Procurar" class="iFile" accept="image/jpeg">
		<input type="hidden" name="file" id="file_true"><br>
		<span class="cArialBold_11_000">Status do Arquivo:</span>
		<input type="text" value="<?php echo $print; ?>"  id="status" class="dFtpStatus"/><br />
		<input type="hidden" name="destino" id="destino">
		<input type="button" value="Enviar" onClick="submitForm();">
	</form>
	</div>
</div>
</body>
</html>
