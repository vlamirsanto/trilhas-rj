/**********************
*  Created By David Wees, October 2, 2006
*  You may use this script in your webpages, but you must keep the 
*  copyright notice below.
*  Copyright 2006, David Wees
*  http://www.unitorganizer.com/myblog
**************************/
var diff = 125;
var positions = new Array();
var verbose = false;
var numbercorrect = 0;

window.setTimeout("$('form').reset()", 10);
window.setTimeout("$('cheating').reset()", 10);
window.setTimeout("checkCookie()", 10);

function setVerbose(el) {
	$('cheating').style.display = "none";
	switch (el.selectedIndex) {
		case 1:
			verbose = true;
			break;
		case 2:
			verbose = false;
			break;
		default:
			verbose = false;
	}
	return;
}

function setDifficulty(el) {
	$('form').style.display = "none";
	switch (el.selectedIndex) {
		case 1:
			diff = 125;
			break;
		case 2:
			diff = 100;
			break;
		case 3:
			diff = 50;
			break;
		case 4:
			diff = 25;
			break;
		default:
			diff = 125;
	}
	return;
}

function selectPicture(el) {
	var num = el.selectedIndex;
	var picture = $("image" + num).value;
	$("imagewrapper").style.backgroundImage = "url(" + picture + ")";
}

function creatediv(top, left, width, height) {
	var newdiv = document.createElement('div');
	newdiv.style.top = top+"px";
	newdiv.style.left = left+"px";
	newdiv.style.width = width+"px";
	newdiv.style.height = height+"px";
	newdiv.style.position = "absolute";
	newdiv.style.display = "block";
	var centerdiv = $('center');
	centerdiv.appendChild(newdiv);
	return newdiv;
}

function createpuzzle() {
	var image = $("imagewrapper").style.backgroundImage;
	$('form').style.display = "none";
	$('cheating').style.display = "none";
	$('save').style.display = "block";
	var newimage;
	var hidediv = $('imagewrapper');
	if (hidediv) {
		hidediv.style.display = 'none';
	}
	var top = 20; // parseInt(hidediv.style.top);
	var left = 20; // parseInt(hidediv.style.left);
	var width = 500; // parseInt(hidediv.style.width);
	var height = 500; // parseInt(hidediv.style.height);	
	createpuzzleboard(image, top, left, width, height);
	var caption = $('caption');
	if (caption) {
		caption.innerHTML = "";
		if (verbose) {
			caption.innerHTML = "Now try and recreate the image by dragging the pieces around.";
		}
	} else {
		if (verbose) {
			$('center').innerHTML = '<div id="caption" style="position: absolute; top: 0px; left: 20px;">Now try and recreate the image by dragging the pieces around.</div>';
		} else {
			$('center').innerHTML = '<div id="caption" style="position: absolute; top: 0px; left: 20px;"></div>';	
		}
		createpuzzleboard(image, top, left, width, height);
	}
	var tmp = new Array();
	var x,y;
	for (i=0;i < (500/diff)*(500/diff);i++) {
		if (!positions[i]) {
			x = Math.floor(Math.random()*400)+200;
			y = Math.floor(Math.random()*200)+650;
			positions[i] = x + "$" + y;
			newimage = creatediv(x, y, diff, diff);
		} else {
			tmp = positions[i].split("$");
			newimage = creatediv(tmp[0], tmp[1], diff, diff);
		}
		newimage.style.backgroundImage = image;
		newimage.id = "("+ ((i%(500/diff))*diff+20) + "," + (Math.floor(i/(500/diff))*diff+20) + ")" + "i" + i;
		newimage.style.backgroundPosition = (i%(500/diff))*(-diff) +"px " + Math.floor(i/(500/diff))*(-diff) + "px";
		Drag.init(newimage);	
	}
	return;
}

function save() {
	var text = positions.join(",");
	if (diff!=25) {
		text = diff + "%" + text;
		jQuery.cookie('the_cookie', '', {expires: 0});
		jQuery.cookie('the_cookie', text, {expires: 30});
		//$('debug').innerHTML = text;
	} else {
		var rtext = text.substring(0,2000);
		var ltext = text.substring(2000);
		rtext = diff + "%" + rtext;
		jQuery.cookie('the_cookie', '', {expires: 0});
		jQuery.cookie('the_cookie', rtext, {expires: 30});
		jQuery.cookie('the_cookie2', '', {expires: 0});
		jQuery.cookie('the_cookie2', ltext, {expires: 30});
		//$('debug').innerHTML = rtext+ltext;
	}
	alert("Saved");
	return;
}

function checkCookie() {
	var rtext = jQuery.cookie('the_cookie');
	var ltext = jQuery.cookie('the_cookie2');
	//$('debug').innerHTML = rtext+ltext;
	if ((rtext)&&(!ltext)) {
		var tmp = rtext.split("%");
		positions = tmp[1].split(",");
		diff = tmp[0];
		createpuzzle('painting.jpg');
	}
	if ((ltext)&&(rtext)) {
		var tmp = rtext.split("%");
		positions = (tmp[1]+ltext).split(",");
		diff = tmp[0];
		createpuzzle('painting.jpg');
	}
	return;
}
function resetGame() {
	for (var i = 0; i < (500/diff)*(500/diff); i++) {
		positions[i] = 0;
	}
	numbercorrect = 0;
	jQuery.cookie('the_cookie', '', {expires: 0});
	jQuery.cookie('the_cookie2', '', {expires: 0});	
	var centerdiv = $('center');
	centerdiv.innerHTML = "<div id=\"imagewrapper\" style=\"position: absolute; top: 20px; left: 20px; width: 500px; height: 500px; display: block; background-image: url('painting.jpg'); background-position: 0px 0px;\" onclick=\"createpuzzle('painting.jpg')\"></div><div id=\"caption\" style=\"position: absolute; top: 0px; left: 20px;\">Click on the image to create a puzzle</div>";
	$('form').reset();
	$('form').style.display = "block";
	$('cheating').reset();
	$('cheating').style.display = "block";
	$('save').style.display = "none";
	//centerdiv.innerHTML = "";
	//createpuzzle('painting.jpg');
	return;	
}

function $(e) {
	return document.getElementById(e);
}

function createpuzzleboard(image, top, left, width, height) {
	var hidediv = $('imagewrapper');
	var puzzlewrapper = creatediv(top, left, width, height);
	for (var i=0;i < (500/diff)*(500/diff);i++) {
		newimage = creatediv( Math.floor(i/(500/diff))*diff + 20, (i%(500/diff))*diff + 20, diff, diff);
		newimage.style.border = "1px dotted #d7e8ff";
		newimage.id = "["+ (i%(500/diff))*(-1*diff) + "," + Math.floor(i/(500/diff))*(-1*diff) + "]";
	}
	return;
}
